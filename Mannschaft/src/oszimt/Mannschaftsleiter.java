package oszimt;

public class Mannschaftsleiter extends Spieler {

	// Attribute
	private String namemannschaft;

	// Konstruktor
	public Mannschaftsleiter() {
	}

	public Mannschaftsleiter(String namemannschaft, String name, int telefonnummer, boolean betragbezahlt) {
		this.namemannschaft = namemannschaft;
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.betragbezahlt = betragbezahlt;
	}

	// Methoden
	public void setNameMannschaft(String namemannschaft) {
		this.namemannschaft = namemannschaft;
	}

	public String getNameMannschaft() {
		return namemannschaft;
	}
}
