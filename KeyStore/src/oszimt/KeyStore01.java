package oszimt;

public class KeyStore01 {

	private String[] key;
	public int currentPos;
	public final int MAXPOSITION = 200;

	public KeyStore01() {
		this.key = new String[MAXPOSITION];
		this.currentPos = 0;

	}

	public KeyStore01(int lenght) {
		this.key = new String[lenght];
		this.currentPos = 0;
	}

	public boolean add(String eingabe) {
		if (currentPos < MAXPOSITION) {
			key[currentPos] = eingabe;
			currentPos++;
			return true;
		} else {
			return false;
		}
	}

	public int indexOf(String eintrag) {
		final int nicht_gefunden = -1;
		for (int i = 0; i < currentPos; i++) {
			if (key[i].equals(eintrag)) {
				return i;
			}

		}
		return nicht_gefunden;
	}

	public void remove(int index) {
		if (index >= 0 && index < currentPos) {

			key[index] = "";
			for (int i = index; i < currentPos; i++) {
				key[i] = key[i + 1];
			}
			currentPos--;
		}
	}

	public String[] getKey() {
		return key;
	}

}
