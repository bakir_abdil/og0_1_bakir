
public class Primzahl {


  public static boolean isPrimzahl(long zahl) {
    boolean primzahl = true;

    for (int i = 2; i < zahl / 2  && primzahl == true; i++) {

      if (zahl % i == 0) {
        primzahl = false;
      }
      
    }
    return primzahl;
  }


}