import java.util.Scanner;

public class KickersTest {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Trainer tr = new Trainer();
		String wahl;

		System.out.println("Bitte Nachname des Trainers eingeben.");
		tr.setNachname(sc.next());
		System.out.println("Biite Vorname des Trainers eingeben.");
		tr.setVorname(sc.next());
		System.out.println("Bitte Telefonnummer eingeben.");
		tr.setTelefonnummer(sc.next());
		System.out.println("Wurde der Jahresbetrag bezahlt. (j/n)");
		wahl = sc.next();
		if (wahl == "j") {
			tr.setJahresbetragbezahlt(true);
		} else {
			tr.setJahresbetragbezahlt(false);
		}
		System.out.println("Bitte Eentschaedigung eingeben.");
		tr.setEntscheadigung(sc.nextDouble());
		System.out.println("Bitte Lizenzklasse eingeben.");
		tr.setLizenzklasse(sc.next().charAt(0));

		System.out.println("Der Trainer " + tr.getVorname() + " " + tr.getNachname() + " hat die Telefonnumer "	+ tr.getTelefonnummer());
		System.out.println("Er hat die Lizenzklasse " + tr.getLizenzklasse() + " und erhält eine Entschädigung von "+ tr.getEntscheadigung());
		System.out.println("Jahresbetrag bezahlt: " + tr.isJahresbetragbezahlt());

	}

}
