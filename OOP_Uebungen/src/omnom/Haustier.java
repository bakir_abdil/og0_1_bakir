package omnom;

public class Haustier {

	// Attribute
	private String name;
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;

	// Verwaltungs-Methoden
	public Haustier(String name) {
		this.name = name;
		hunger = 100;
		muede = 100;
		zufrieden = 100;
		gesund = 100;
	}

	public String getName() {
		return name;
	}

	public void setHunger(int hunger) {
		this.hunger = hunger;
	}

	public int getHunger() {
		return hunger;
	}

	public void setMuede(int muede) {
		this.muede = muede;
	}

	public int getMuede() {
		return muede;
	}

	public void setZufrieden(int zufrieden) {
		this.zufrieden = zufrieden;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setGesund(int gesund) {
		this.gesund = gesund;
	}

	public int getGesund() {
		return gesund;
	}

	// Methoden Interaktionen

	public void fuettern(int zahl) {
		if (hunger < 0) {
			hunger = 0 + zahl;
		} else {
			hunger = hunger + zahl;
			if (hunger > 100) {
				hunger = 100;
			}
		}
	}

	public void schlafen(int zahl) {
		if (muede < 0) {
			muede = 0 + zahl;
		} else {
			muede = muede + zahl;
			if (muede > 100) {
				muede = 100;
			}
		}
	}

	public void spielen(int zahl) {
		if (zufrieden < 0) {
			zufrieden = 0 + zahl;
		} else {
			zufrieden = zufrieden + zahl;
			if (zufrieden > 100) {
				zufrieden = 100;
			}
		}
	}

	public void heilen() {
		gesund = 100;
	}

}
