package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pilot {

	// Attribute
	private String grad;
	private String name;
	private double posx;
	private double posy;

	// Methoden
	public Pilot() {
	}

	public void setGrad(String grad) {
		this.grad = grad;
	}

	public String getGrad() {
		return grad;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPosX(double posx) {
		this.posx = posx;
	}

	public double getPosX() {
		return posx;
	}

	public void setPosY(double posy) {
		this.posy = posy;
	}
	public double getPosY() {
		return posy;
	}
}
