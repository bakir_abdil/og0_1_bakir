
public class Schiedsrichter extends Mannschaftsdingens{

	//Attribute
	private int gepfiffenespiele;
	
	//Konstruktor
	public Schiedsrichter() {}

	//Methoden
	public int getGepfiffenespiele() {
		return gepfiffenespiele;
	}

	public void setGepfiffenespiele(int gepfiffenespiele) {
		this.gepfiffenespiele = gepfiffenespiele;
	}
	
	
}
