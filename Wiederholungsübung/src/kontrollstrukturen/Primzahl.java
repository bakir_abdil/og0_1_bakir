package kontrollstrukturen;

import java.util.Scanner;

public class Primzahl {

  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    System.out.println("Geben sie eine Zahl zur Primzahlkontrolle ein.");
    long zahl = sc.nextLong();
    boolean primzahl = true;
    sc.close();

    for (int i = 2; i < zahl; i++) {     
      if (zahl % i == 0) {             //Bestimmen ob Rest �brig bleibt
        primzahl = false;              //Falls gesetzt fals kein Rest vorhanden bzw. keine Primzahl
      }
    }

    if (primzahl == true) {
      System.out.println("Die Zahl " + zahl + " ist eine Primzahl.");
    } else {
      System.out.println("Die Zahl " + zahl + " ist keine Primzahl.");
    }
  }
}