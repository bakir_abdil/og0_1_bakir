package OSZIMT;

public class Addon {

	// Attribute
	private int idnummer;
	private String bezeichnung;
	private double preis;
	private int bestand;
	private int maxbestand;

	public Addon() {

	}

	public Addon(int idnummer, String bezeichnung, double preis, int bestand, int maxbestand) {
		this.idnummer = idnummer;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.bestand = bestand;
		this.maxbestand = maxbestand;
	}

	public int getIdnummer() {
		return idnummer;
	}

	public void setIdnummer(int idnummer) {
		this.idnummer = idnummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getMaxbestand() {
		return maxbestand;
	}

	public void setMaxBestand(int maxbestand) {
		this.maxbestand = maxbestand;
	}

	public void veraenderBestand(char plusOderMinus, int zahl) {
		if (plusOderMinus == '+') {
			this.bestand = this.bestand + zahl;
		} else if (plusOderMinus == '-') {
			this.bestand = this.bestand - zahl;
		}
	}
}
