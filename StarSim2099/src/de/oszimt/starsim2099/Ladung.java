package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	private String typ;
	private int masse;
	private double posx;
	private double posy;

	// Methoden
	public Ladung() {
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTyp() {
		return typ;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public int getMasse() {
		return masse;
	}

	public void setPosX(double posx) {
		this.posx = posx;
	}

	public double getPosX() {
		return posx;
	}

	public void setPosY(double posy) {
		this.posy = posy;
	}

	public double getPosY() {
		return posy;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}