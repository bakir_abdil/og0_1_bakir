package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	
	public static long findPromoCode(long[] list, long searchValue) {

		return searchPromoCode(list, 0, list.length - 1, searchValue);
	}/// **/

	public static long searchPromoCode(long[] list, long low, long high, long searchvalue) {

		if (low > high) {
			return NOT_FOUND;
		}
		long m = low + (high - low) / 2;
		if (searchvalue < list[(int) m]) {
			return searchPromoCode(list, low, m - 1, searchvalue);
		}
		if (searchvalue > list[(int) m]) {
			return searchPromoCode(list, m + 1, high, searchvalue);
		}
		return m;
	}

}
