
public class Spieler extends Mannschaftsdingens {

	//Attribute
	private String spielposition;
	private int trikotnummer; 
	
	//Konstruktor
	public Spieler() {}

	//Methoden
	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	
	
}
