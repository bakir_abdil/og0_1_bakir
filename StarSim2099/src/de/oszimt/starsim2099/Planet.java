package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet {

	// Attribute
	private int anzahlhafen;
	private String name;
	private double posx;
	private double posy;

	// Methoden
	public Planet() {
	}

	public void setAnzahlHafen(int anzahlhafen) {
		this.anzahlhafen = anzahlhafen;
	}
	public int getAnzahlHafen() {
		return anzahlhafen;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setPosX(double posx) {
		this.posx = posx;
	}
	public double getPosX() {
		return posx;
	}
	public void setPosY(double posy) {
		this.posy = posy;
	}
	public double getPosY() {
		return posy;
	}
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
