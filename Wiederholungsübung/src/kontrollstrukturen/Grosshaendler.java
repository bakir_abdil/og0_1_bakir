package kontrollstrukturen;

import java.util.Scanner;

public class Grosshaendler {

  public static void main(String[] args) {
    System.out.println("Wie viel PC-M�use wollen sie kaufen?");
    Scanner sc = new Scanner(System.in);
    int anzahl = sc.nextInt();
    int preis = 7;
    double rechnung;
    sc.close();
    
    if (anzahl < 10) {                         //falls < 10
    rechnung = (anzahl * preis * 1.19) +10;    //rechungs formel
    }
    else {
    rechnung = anzahl * preis * 1.19;
    }
    System.out.println("Die Rechnung inkl. MwSt. f�r die PC-M�use betr�gt " + rechnung);
  }

}