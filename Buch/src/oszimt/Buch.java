package oszimt;

public class Buch {

	private String autor;
	private String titel;
	private String isbn;

	public Buch() {}
	
	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public int compareTo(Buch b){
		return this.isbn.compareTo(b.getIsbn());
		
	}
	
	public String toString(){
		return "[ Isbn: "+this.isbn + ", "+ "Titel: "+this.titel + ", "+ "Autor: " + this.autor + " ]";
	}
	

}
