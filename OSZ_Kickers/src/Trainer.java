
public class Trainer extends Mannschaftsdingens {

	//Attribute
	private double entscheadigung;
	private char lizenzklasse;
	
	//Konstruktor
	public Trainer() {}

	//Methoden
	public double getEntscheadigung() {
		return entscheadigung;
	}

	public void setEntscheadigung(double entscheadigung) {
		this.entscheadigung = entscheadigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	
	
}
