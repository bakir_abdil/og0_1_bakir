package oszimt;

public class Quersumme {

	public static int quersummeBerechnen (String zahl) {
		int temp = 0;
		if (zahl.length() == 1) {
			return temp = Integer.parseInt(zahl);
		}
		temp = Character.getNumericValue(zahl.charAt(0));
		return temp + quersummeBerechnen(zahl.substring(1));
	}
}
