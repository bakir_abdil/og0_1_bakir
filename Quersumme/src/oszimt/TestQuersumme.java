package oszimt;

import java.util.Scanner;

public class TestQuersumme {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Zahl eingeben:");
		String zahl = sc.next();
		System.out.println("Quersumme von " + zahl + ": " + Quersumme.quersummeBerechnen(zahl));
	}

}
