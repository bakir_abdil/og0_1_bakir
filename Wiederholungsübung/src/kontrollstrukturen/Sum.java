package kontrollstrukturen;

import java.util.Scanner;

public class Sum {

	public static void main(String[] args) {
		System.out.println("Bitte geben sie den Grenzwert ein.");
		Scanner sc = new Scanner(System.in);
		int grenzwert = sc.nextInt();
		sc.close();
		int zahl = 2;
		int ergebnis = 0;

		while (zahl <= grenzwert) {          //
			ergebnis = ergebnis + zahl;      //zahl pro durchlauf addieren
			zahl = zahl + 2;                 
		}
		System.out.println("Das ergebnis lautet " + ergebnis);
	}
}