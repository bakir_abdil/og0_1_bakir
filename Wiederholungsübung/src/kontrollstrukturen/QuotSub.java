package kontrollstrukturen;

import java.util.Scanner;

public class QuotSub {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte Dividend eingeben.");
		int dividend = sc.nextInt();
		System.out.println("Bitte Divisor eingeben.");
		int divisor = sc.nextInt();
		int quotient = 0;
		sc.close();

		while (dividend >= divisor) {         // Schleife falls divisor <= dividend
			dividend = dividend - divisor;    // dividend subtrahieren durch divisor
			quotient++;                       // Quotient +1
		}

		System.out.println("Ganzzahliger Quotient: " + quotient);
		System.out.println("Ganzzahliger Rest: " + dividend);

	}
}