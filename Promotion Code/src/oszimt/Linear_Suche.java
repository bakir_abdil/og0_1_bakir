package oszimt;

public class Linear_Suche {

	//private final int nicht_gefunden = -1;

	public static int sucheWert(int[] zahlen, int gesuchtezahl) {

		final int nicht_gefunden = -1;
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i] == gesuchtezahl) {
				return i;
			}

		}
		return nicht_gefunden;
	}
}
