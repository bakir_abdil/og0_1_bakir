package oszimt;

public class Interpolations_Suche {

	// private final int nicht_gefunden = -1;

	public static int sucheWert(int[] zahlen, int gesuchtezahl) {

		final int nicht_gefunden = -1;
		int von = 0;
		int bis = (zahlen.length - 1);
		int t;
		
		while (von != bis) {

			if (zahlen[von] < zahlen[bis]) {
				t = von + (bis - von) * ((gesuchtezahl - zahlen[von]) / (zahlen[bis] - zahlen[von]));
				System.out.println(t);
				if (zahlen[t] == gesuchtezahl) {
					return t;
				} else if (gesuchtezahl < zahlen[t]) {
					bis = t - 1;
				} else {
					von = t + 1;
				}
			}
		}
		if (gesuchtezahl == zahlen[von]) {
			return von;
		}

		return nicht_gefunden;

	}

}
