package oszimt;

public class Trainer extends Mannschaftsdingens {

	// Attribute
	private char lizenzklasse;

	// Konstruktor
	public Trainer() {
	}

	public Trainer(char lizenzklasse, String name, int telefonnummer, boolean betragbezahlt) {
		this.lizenzklasse = lizenzklasse;
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.betragbezahlt = betragbezahlt;
	}

	// Methoden
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}
}
