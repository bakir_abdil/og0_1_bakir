package oszimt;

public abstract class Test {

	//Attribute
	protected boolean aktiv;
	protected double ergebnis;
	
	//Methoden
	protected abstract void warten();
	
	
	public abstract double getErgebnis(); 
	
	public abstract boolean isAktiv(); 
	
	public abstract void starten(); 
	
	public abstract void stoppen(); 
	
	public abstract void zeigeHilfe(); 
	
}
