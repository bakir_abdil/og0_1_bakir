
public class Mannschaftsleiter extends Spieler {

	//Attribute
	private String mannschaftsname;
	private int rabatt;
	
	//Konstruktor
	public Mannschaftsleiter() {}

	//Methoden
	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	
}
