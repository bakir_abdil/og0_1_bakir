package oszimt;

public class Spieler extends Mannschaftsdingens {

	// Attribute
	protected int trikotnummer;
	protected String spielposition;

	// Konstruktor
	public Spieler() {
	}

	public Spieler(int trikotnummer, String spielposition, String name, int telefonnummer, boolean betragbezahlt) {
		this.spielposition = spielposition;
		this.trikotnummer = trikotnummer;
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.betragbezahlt = betragbezahlt;
	}

	// Methoden
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public String getSpielposition() {
		return spielposition;
	}
}
