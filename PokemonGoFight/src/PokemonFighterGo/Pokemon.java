package PokemonFighterGo;
import java.util.Arrays;

public class Pokemon {

	// Attribute
	private String name;
	private String typ1;
	private String typ2;
	private String fangDatum;
	private double groesse;
	private double gewicht;
	private int benoetigteBonbons;
	private int benoetigteSternstaub;
	private int kp;
	private int wp;
	private boolean isfavorite;
	private Abilitys[] ability;

	// Konstruktor
	public Pokemon(String name, String typ1, String typ2, String fangDatum, double groesse, double gewicht,
			int benoetigteBonbons, int benoetigteSternstaub, int kp, int wettkampfpunkte, boolean isfavorite,
			Abilitys[] abilitys) {
		this.name = name;
		this.typ1 = typ1;
		this.typ2 = typ2;
		this.fangDatum = fangDatum;
		this.groesse = groesse;
		this.gewicht = gewicht;
		this.benoetigteBonbons = benoetigteBonbons;
		this.benoetigteSternstaub = benoetigteSternstaub;
		this.kp = kp;
		this.wp = wettkampfpunkte;
		this.isfavorite = isfavorite;
		this.ability = ability;
	}

	// Setter und Getter Methoden
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTyp1() {
		return typ1;
	}

	public void setTyp1(String typ1) {
		this.typ1 = typ1;
	}

	public String getTyp2() {
		return typ2;
	}

	public void setTyp2(String typ2) {
		this.typ2 = typ2;
	}

	public String getFangDatum() {
		return fangDatum;
	}

	public void setFangDatum(String fangDatum) {
		this.fangDatum = fangDatum;
	}

	public double getGroesse() {
		return groesse;
	}

	public void setGroesse(double groesse) {
		this.groesse = groesse;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public int getBenoetigteBonbons() {
		return benoetigteBonbons;
	}

	public void setBenoetigteBonbons(int benoetigteBonbons) {
		this.benoetigteBonbons = benoetigteBonbons;
	}

	public int getBenoetigteSternstaub() {
		return benoetigteSternstaub;
	}

	public void setBenoetigteSternstaub(int benoetigteSternstaub) {
		this.benoetigteSternstaub = benoetigteSternstaub;
	}

	public int getKp() {
		return kp;
	}

	public void setKp(int kp) {
		this.kp = kp;
	}

	public int getWp() {
		return wp;
	}

	public void setWp(int wettkampfpunkte) {
		this.wp = wettkampfpunkte;
	}

	public boolean isIsfavorite() {
		return isfavorite;
	}

	public void setIsfavorite(boolean isfavorite) {
		this.isfavorite = isfavorite;
	}

	public Abilitys[] getAbility() {
		return ability;
	}

	public void setAbility(Abilitys[] ability) {
		this.ability = ability;
	}

	// toString Methode
	public String toString() {
		return "Pokemon [name=" + name + ", typ1=" + typ1 + ", typ2=" + typ2 + ", fangDatum=" + fangDatum + ", groesse="
				+ groesse + ", gewicht=" + gewicht + ", benoetigteBonbons=" + benoetigteBonbons
				+ ", benoetigteSternstaub=" + benoetigteSternstaub + ", kp=" + kp + ", wp=" + wp + ", isfavorite="
				+ isfavorite + ", ability=" + Arrays.toString(ability) + "]";
	}

}
