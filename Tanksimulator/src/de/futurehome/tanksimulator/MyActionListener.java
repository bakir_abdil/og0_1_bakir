package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javafx.scene.control.Slider;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = fuellstand + 5;
			if (fuellstand > 100) {
				fuellstand = 100;
			}
			f.myTank.setFuellstand(fuellstand);
		
			f.lblFuellstand.setText("" + fuellstand);
			f.lblFuellstandp.setText(fuellstand + "% ");
		}
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			int verbrauch = f.slSlider.getValue();
			fuellstand = fuellstand - verbrauch;
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand);
			f.lblFuellstandp.setText(fuellstand + "% ");
		}
		if (obj == f.btnZuruecksetzen) {
			double fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand);
			f.lblFuellstandp.setText(fuellstand + "% ");
		}

	}
}