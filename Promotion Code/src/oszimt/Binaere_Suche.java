package oszimt;

public class Binaere_Suche {

	public static long sucheWert(long[] list, long searchValue) {

		return searchPromoCode(list, 0, list.length - 1, searchValue);
	}

	public static long searchPromoCode(long[] list, long low, long high, long searchvalue) {

		final int nicht_gefunden = -1;
		if (low > high) {
			return nicht_gefunden;
		}
		long m = low + (high - low) / 2;
		if (searchvalue < list[(int) m]) {
			return searchPromoCode(list, low, m - 1, searchvalue);
		}
		if (searchvalue > list[(int) m]) {
			return searchPromoCode(list, m + 1, high, searchvalue);
		}
		return m;
	}
}
