
import java.util.Scanner;

public class TestPrimzahl {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		long zahl;
		boolean erg;
		long time;
		long timefinish;

		System.out.println("Wie viel Zahlen sollen eingegeben werden?"); // Festlegen von Arraygr��e
		long[] zahlenliste = new long[sc.nextInt()];

		System.out.println("Bitte Zahlen eingeben: "); // Zahlen nacheinander
		for (int i = 0; i < zahlenliste.length; i++) {
			zahlenliste[i] = sc.nextLong();
		}
		System.out.println("Zahl \t isPrimzahl \t Zeit \t"); //Tabelle erstellen

		for (int i = 0; i < zahlenliste.length; i++) {
			time = System.currentTimeMillis(); //jetzige Zeit speichern
			erg = Primzahl.isPrimzahl(zahlenliste[i]);
			timefinish = System.currentTimeMillis() - time; // jetzige Zeit mit der gespeicherten subtrahieren
			System.out.println(zahlenliste[i] + "\t" + erg + "\t" + timefinish + "ms" + "\t");
		}

	}
}