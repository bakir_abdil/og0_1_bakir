package oszimt;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.CardLayout;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import java.awt.Dimension;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;

public class gui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gui frame = new gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public gui() {
		setTitle("Tauglichkeitstester");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 572, 484);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		
		JButton btnStart = new JButton("Start");
		btnStart.setPreferredSize(new Dimension(100, 30));
		panel_1.add(btnStart);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.EAST);
		
		JButton btnStop = new JButton("Stop");
		btnStop.setPreferredSize(new Dimension(100, 30));
		panel_2.add(btnStop);
		
		JPanel pnlRdbtn = new JPanel();
		panel.add(pnlRdbtn, BorderLayout.WEST);
		pnlRdbtn.setLayout(new BorderLayout(0, 0));
		
		JRadioButton rdbtnReaktion = new JRadioButton("Reaktion");
		pnlRdbtn.add(rdbtnReaktion, BorderLayout.NORTH);
		
		JRadioButton rdbtnEinschaetzung = new JRadioButton("Konzentration");
		pnlRdbtn.add(rdbtnEinschaetzung, BorderLayout.SOUTH);
		
		JRadioButton rdbtnKonzentration = new JRadioButton("Einsch\u00E4tzung");
		pnlRdbtn.add(rdbtnKonzentration, BorderLayout.WEST);
		
		JPanel cardlayout = new JPanel();
		contentPane.add(cardlayout, BorderLayout.CENTER);
		cardlayout.setLayout(new CardLayout(0, 0));
		
		JPanel pnlReaktion = new JPanel();
		cardlayout.add(pnlReaktion, "name_4982037024700");
		pnlReaktion.setLayout(new BorderLayout(0, 0));
		
		JTextArea txtrReaktion = new JTextArea();
		txtrReaktion.setWrapStyleWord(true);
		txtrReaktion.setLineWrap(true);
		txtrReaktion.setPreferredSize(new Dimension(250, 22));
		txtrReaktion.setText("Reaktion\r\n\r\nNach dem Best\u00E4tigen der Start-Schaltfl\u00E4che erscheint eine Ampel, die rot zeigt. Beim Umspringen auf gr\u00FCn muss die Stopp-Schaltfl\u00E4che gedr\u00FCckt werden. Es wird anschlie\u00DFend die Reaktionszeit in s angezeigt.");
		pnlReaktion.add(txtrReaktion, BorderLayout.EAST);
		
		JPanel panel_3 = new JPanel();
		pnlReaktion.add(panel_3, BorderLayout.SOUTH);
		
		JLabel lblNewLabel = new JLabel("Auswertung");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_3.add(lblNewLabel);
		
		JPanel panel_4 = new JPanel();
		pnlReaktion.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5, BorderLayout.EAST);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setSize(new Dimension(50, 50));
		lblNewLabel_1.setPreferredSize(new Dimension(100, 100));
		lblNewLabel_1.setIcon(new ImageIcon(gui.class.getResource("/bilder/red_circle.png")));
		panel_5.add(lblNewLabel_1);
		
		JPanel pnlEinschaetzung = new JPanel();
		cardlayout.add(pnlEinschaetzung, "name_4989795770100");
		
		JPanel pnlKonzentration = new JPanel();
		cardlayout.add(pnlKonzentration, "name_4995607741500");
	}

}
