package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	private String typ;
	private String antrieb;
	private int maxladekapazitaet;
	private int winkel;
	private double posx;
	private double posy;

	// Methoden
	public Raumschiff() {
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getTyp() {
		return typ;
	}
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	public String getAntrieb() {
		return antrieb;
	}
	public void setMaxLadekapazitaet(int maxladekapazitaet) {
		this.maxladekapazitaet = maxladekapazitaet;
	}
	public int getMaxLadekapazitaet() {
		return maxladekapazitaet;
	}
	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}
	public int getWinkel() {
		return winkel;
	}
	public void setPosX(double posx) {
		this.posx = posx;
	}
	public double getPosX() {
		return posx;
	}
	public void setPosY(double posy) {
		this.posy = posy;
	}
	public double getPosY() {
		return posy;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { { '\0', '\0', '_', '\0', '\0' }, { '\0', '/', 'X', '\\', '\0' },
				{ '\0', '{', 'X', '}', '\0' }, { '\0', '{', 'X', '}', '\0' }, { '/', '_', '_', '_', '\\' }, };
		return raumschiffShape;
	}

}
