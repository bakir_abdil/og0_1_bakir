package kontrollstrukturen;

import java.util.Scanner;

public class BMI {

  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    System.out.println("Bitte K�rpergewicht eingeben.");
    double gewicht = sc.nextDouble();
    System.out.println("Bitte K�rpergr��e eingeben.");
    double groesse = sc.nextDouble();
    System.out.println("M�nlich(m) oder Weiblich(w) eingeben.");
    char geschlecht = sc.next().charAt(0);
    double bmi = gewicht / groesse;               // BMI wird berechnet
    System.out.println("BMI : " + bmi);
    sc.close();

    if (geschlecht == 'm') {                      // if-Anweisung f�r M�nlich
      if (bmi < 20) {
        System.out.println("Ergebnis: Untergewicht");
      } else if (bmi > 25) {
        System.out.println("Ergebnis: �bergewicht");
      } else {
        System.out.println("Normalgewicht");
      }
    }

    else if (geschlecht == 'w') {                 // if-Anweisung f�r Weiblich
      if (bmi < 19) {
        System.out.println("Ergebnis: Untergewicht");
      } else if (bmi > 24) {
        System.out.println("Ergebnis: �bergewicht");
      } else {
        System.out.println("Normalgewicht");
      }

    }
  }
}