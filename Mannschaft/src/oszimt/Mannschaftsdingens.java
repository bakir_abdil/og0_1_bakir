package oszimt;

public class Mannschaftsdingens {

	// Attribute
	protected String name;
	protected int telefonnummer;
	protected boolean betragbezahlt;

	// Methoden
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public int getTelefonnummer() {
		return telefonnummer;
	}

	public void setBetragbezahlt(boolean betragbezahlt) {
		this.betragbezahlt = betragbezahlt;
	}

	public boolean getBetragbezahlt() {
		return betragbezahlt;
	}
}
