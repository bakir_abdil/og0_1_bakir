
public class Mannschaftsdingens {

	//Attrtibute
	private String vorname;
	private String nachname;
	private String telefonnummer;
	private boolean jahresbetragbezahlt;

	// Konstruktor
	public Mannschaftsdingens() {
	}

	//Methoden
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isJahresbetragbezahlt() {
		return jahresbetragbezahlt;
	}

	public void setJahresbetragbezahlt(boolean jahresbetragbezahlt) {
		this.jahresbetragbezahlt = jahresbetragbezahlt;
	}

	
}
