package oszimt;

public class Schiedsrichter extends Mannschaftsdingens {

	// Attribute
	private int gepfiffenespiele;

	// Konstruktor
	public Schiedsrichter() {
	}

	public Schiedsrichter(int gepfiffenespiele, String name, int telefonnummer, boolean betragbezahlt) {
		this.gepfiffenespiele = gepfiffenespiele;
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.betragbezahlt = betragbezahlt;
	}

	// Methoden
	public void setGepfiffenespiele(int gepfiffenespiele) {
		this.gepfiffenespiele = gepfiffenespiele;
	}

	public int getGepfiffenespiele() {
		return gepfiffenespiele;
	}
}
